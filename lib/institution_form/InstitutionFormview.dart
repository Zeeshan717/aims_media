import 'dart:io';

import 'package:aims_media/home/home_view.dart';
import 'package:aims_media/institution_form/institution_form_controller.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:image_picker/image_picker.dart';

import '../individual_form/individual_form_controller.dart';

enum ImageSourceType { gallery, camera }

class InstitutionFormView extends StatefulWidget {
  const InstitutionFormView({Key? key}) : super(key: key);

  @override
  State<InstitutionFormView> createState() => _InstitutionFormViewState();
}

class _InstitutionFormViewState extends State<InstitutionFormView> {
  var controller = Get.put(InstitutionFormController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.chevron_left_outlined,
              color: Colors.black,
            ),
            onPressed: () {
              Get.back();
              Get.back();
            },
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            'Registration',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                letterSpacing: 2.5),
          )),
      body: Container(
        padding: EdgeInsets.all(16),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("Institute Detail:",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff989a9c),
                      )),
                ],
              ),
              SizedBox(height: 20),

              Visibility(
                  visible: false,
                  child: Column(
                    children: [
                      Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                          decoration: BoxDecoration(
                            border:
                                Border.all(width: 1, color: Color(0xff0aaaa0)),
                          ),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    "Institution Name : ",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "Maharshi Arvind univarsity-MAU",
                                    style: TextStyle(fontSize: 10),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Authorized Person Name : ",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "Dr. Shanu Kandelwal",
                                    style: TextStyle(fontSize: 10),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Email-Id : ",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "mahrshi@gmail.com",
                                    style: TextStyle(fontSize: 10),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Contact No : ",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "5895959595",
                                    style: TextStyle(fontSize: 10),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Address & Location : ",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "Jaipur Rajasthan",
                                    style: TextStyle(fontSize: 10),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Number Of Participant : ",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "1/14",
                                    style: TextStyle(fontSize: 10),
                                  )
                                ],
                              ),
                            ],
                          )),
                      SizedBox(height: 20),
                      Container(
                          constraints: BoxConstraints(maxHeight: 200),
                          // height: 300,
                          padding:
                              EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                          decoration: BoxDecoration(
                            border:
                                Border.all(width: 1, color: Color(0xff0aaaa0)),
                          ),
                          child: DataTable2(
                              columnSpacing: 12,
                              horizontalMargin: 0,
                              minWidth: 200,
                              columns: [
                                DataColumn2(
                                  fixedWidth: 50,
                                  label: Text('SNo.'),
                                  size: ColumnSize.L,
                                ),
                                DataColumn(
                                  label: Text('Name'),
                                ),
                                DataColumn(
                                  label: Text('Phone No.'),
                                ),
                                DataColumn(
                                  label: Text('Registration Id'),
                                ),
                              ],
                              rows: List<DataRow>.generate(
                                  10,
                                  (index) => DataRow(cells: [
                                        DataCell(Text("${index + 1}")),
                                        DataCell(Text("Anshul Sharma")),
                                        DataCell(Text("985658792")),
                                        DataCell(Text("225498"))
                                      ])))),
                      SizedBox(height: 20),
                    ],
                  )),

              Visibility(
                visible: !controller.flag,
                child: Column(
                  children: [
                    // institution Name
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Institution Name",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 12),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        decoration: BoxDecoration(
                          border:
                              Border.all(width: 1, color: Color(0xff979BA3)),
                        ),
                        child: TextField(
                          controller: controller.institutionName,
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w400),
                          cursorColor: Colors.black,
                          cursorWidth: 1.5,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            hintText: "Institution Name",
                            hintStyle:
                                TextStyle(fontSize: 12, letterSpacing: 0.5),
                          ),
                        )),
                    SizedBox(height: 20),

                    // Authorized Person Name
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Authorized Person Name",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 12),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        decoration: BoxDecoration(
                          border:
                              Border.all(width: 1, color: Color(0xff979BA3)),
                        ),
                        child: TextField(
                          controller: controller.authPersonName,
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w400),
                          cursorColor: Colors.black,
                          cursorWidth: 1.5,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            hintText: "Authorized Person Name",
                            hintStyle:
                                TextStyle(fontSize: 12, letterSpacing: 0.5),
                          ),
                        )),
                    SizedBox(height: 20),

                    // Email-ld
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Email-ld",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 12),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        decoration: BoxDecoration(
                          border:
                              Border.all(width: 1, color: Color(0xff979BA3)),
                        ),
                        child: TextField(
                          controller: controller.iemail,
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w400),
                          cursorColor: Colors.black,
                          cursorWidth: 1.5,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            hintText: "Email-ld",
                            hintStyle:
                                TextStyle(fontSize: 12, letterSpacing: 0.5),
                          ),
                        )),
                    SizedBox(height: 20),

                    // Contact No
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Contact No",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 12),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        decoration: BoxDecoration(
                          border:
                              Border.all(width: 1, color: Color(0xff979BA3)),
                        ),
                        child: TextField(
                          keyboardType: TextInputType.number,
                          controller: controller.icontactNo,
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w400),
                          cursorColor: Colors.black,
                          cursorWidth: 1.5,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            hintText: "Contact No",
                            hintStyle:
                                TextStyle(fontSize: 12, letterSpacing: 0.5),
                          ),
                        )),
                    SizedBox(height: 20),

                    // Address & Location
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Address & Location",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 12),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        decoration: BoxDecoration(
                          border:
                              Border.all(width: 1, color: Color(0xff979BA3)),
                        ),
                        child: TextField(
                          controller: controller.addressLocation,
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w400),
                          cursorColor: Colors.black,
                          cursorWidth: 1.5,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            hintText: "Address & Location",
                            hintStyle:
                                TextStyle(fontSize: 12, letterSpacing: 0.5),
                          ),
                        )),
                    SizedBox(height: 20),

                    // Number of participants
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Number of participants",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 12),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        decoration: BoxDecoration(
                          border:
                              Border.all(width: 1, color: Color(0xff979BA3)),
                        ),
                        child: TextField(
                          controller: controller.noOfParticipation,
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w400),
                          cursorColor: Colors.black,
                          cursorWidth: 1.5,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            hintText: "Number of participants",
                            hintStyle:
                                TextStyle(fontSize: 12, letterSpacing: 0.5),
                          ),
                        )),
                    SizedBox(height: 20),
                  ],
                ),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("Participant Detail",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff989a9c),
                      )),
                ],
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Participant Name",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.participantName,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "Participant Name",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Guardian Name",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.guardianName,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "Guardian Name",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Email-Id",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.pemail,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "Email-Id",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Contact No",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    keyboardType: TextInputType.number,
                    controller: controller.pcontaxtNo,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "Contact No",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Age",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  border: Border.all(
                    width: 1,
                    color: Color(0xff000000).withOpacity(.20),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: DropdownButton<String>(
                        iconSize: 0,
                        value: controller.dropdownValue,
                        elevation: 16,
                        style: TextStyle(color: Colors.black),
                        underline: Container(
                          height: 0,
                          color: Colors.deepPurpleAccent,
                        ),
                        focusColor: Color(0xfffafafa).withOpacity(.30),
                        autofocus: false,
                        onChanged: (String? value) {
                          // This is called when the user selects an item.
                          setState(() {
                            controller.dropdownValue = value!;
                          });
                        },
                        items: [
                          'Age (10 to 45 years)',
                          ...controller.getAgeArray()
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Icon(Icons.arrow_drop_down_outlined),
                  ],
                ),
              ),
              // Container(
              //     padding: EdgeInsets.symmetric(horizontal: 16),
              //     decoration: BoxDecoration(
              //       border: Border.all(width: 1, color: Color(0xff979BA3)),
              //     ),
              //     child: const TextField(
              //       style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
              //       cursorColor: Colors.black,
              //       cursorWidth: 1.5,
              //       decoration: InputDecoration(
              //         border: InputBorder.none,
              //         enabledBorder: InputBorder.none,
              //         focusedBorder: InputBorder.none,
              //         hintText: "Age",
              //         hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
              //       ),
              //     )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "State Location",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.stateLocation,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "State Location",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Upload Aadhar Card",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  controller
                      .uploadFromGallery1()
                      .then((value) => setState(() {}));
                },
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Color(0xff979BA3)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 14),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(" Tap To Upload Image"),
                          controller.image1 != null
                              ? Image.file(
                                  controller.image1!,
                                  width: 100.0,
                                  height: 100.0,
                                  fit: BoxFit.fitHeight,
                                )
                              : Container(
                                  child: Icon(Icons.image),
                                )
                        ],
                      ),
                    )),
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Statement of Purpose",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.purpose,
                    keyboardType: TextInputType.multiline,
                    minLines: 1, //Normal textInputField will be displayed
                    maxLines: 5, // when user presses enter it will adapt to it
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText:
                          "Statement of Purpose (SOP) (Why do you want to register in WWAD 2022?) (100 Words)",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "About Yourself",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.aboutYourSelf,
                    minLines: 1,
                    maxLines: 5,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText:
                          "About Yourself (Please cover your skills, hobbies, area of interest and what you are currently doing) (100 Words)",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Have you participated in any talent show/event in the past?",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  border: Border.all(
                    width: 1,
                    color: Color(0xff000000).withOpacity(.20),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: DropdownButton<String>(
                        iconSize: 0,
                        value: controller.dropdownValue2,
                        elevation: 16,
                        style: TextStyle(color: Colors.black),
                        underline: Container(
                          height: 0,
                          color: Colors.deepPurpleAccent,
                        ),
                        focusColor: Color(0xfffafafa).withOpacity(.30),
                        autofocus: false,
                        onChanged: (String? value) {
                          // This is called when the user selects an item.
                          setState(() {
                            controller.dropdownValue2 = value!;
                          });
                        },
                        items: ['No', 'Yes']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Icon(Icons.arrow_drop_down_outlined),
                  ],
                ),
              ),

              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Talent Show Description",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.talentShowDes,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "Talent show/event Description",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Please mention Disability Category",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  border: Border.all(
                    width: 1,
                    color: Color(0xff000000).withOpacity(.20),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: DropdownButton<String>(
                        iconSize: 0,
                        value: controller.dropdownValue3,
                        elevation: 16,
                        style: TextStyle(color: Colors.black),
                        underline: Container(
                          height: 0,
                          color: Colors.deepPurpleAccent,
                        ),
                        focusColor: Color(0xfffafafa).withOpacity(.30),
                        autofocus: false,
                        onChanged: (String? value) {
                          // This is called when the user selects an item.
                          setState(() {
                            controller.dropdownValue3 = value!;
                          });
                        },
                        items: [
                          'Please mention Disability Category',
                          'Autism Spectrum Disorder',
                          'Down Syndrome',
                          'Cerebral Palsy',
                          'Locomotor Disability',
                          'Dwarfism',
                          'Visually Impaired/Blindness',
                          'Acid Victims',
                          'Intellectual Disability',
                          'Multiple Disabilities including deafblindness',
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Icon(Icons.arrow_drop_down_outlined),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "UUID Card",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  controller
                      .uploadFromGallery2()
                      .then((value) => setState(() {}));
                },
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Color(0xff979BA3)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 14),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(" Tap To Upload Image"),
                          controller.image2 != null
                              ? Image.file(
                                  controller.image2!,
                                  width: 100.0,
                                  height: 100.0,
                                  fit: BoxFit.fitHeight,
                                )
                              : Container(
                                  child: Icon(Icons.image),
                                )
                        ],
                      ),
                    )),
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Medical/Health Issue, if any",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.healthIssue,
                    minLines: 1,
                    maxLines: 5,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "Medical/Health Issue, if any",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Assistance",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.assistance,
                    minLines: 1,
                    maxLines: 5,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText:
                          "If any specific assistance required, please mention",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "How do you know about WWAD 2022",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  border: Border.all(
                    width: 1,
                    color: Color(0xff000000).withOpacity(.20),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: DropdownButton<String>(
                        iconSize: 0,
                        value: controller.dropdownValue4,
                        elevation: 16,
                        style: TextStyle(color: Colors.black),
                        underline: Container(
                          height: 0,
                          color: Colors.deepPurpleAccent,
                        ),
                        focusColor: Color(0xfffafafa).withOpacity(.30),
                        autofocus: false,
                        onChanged: (String? value) {
                          // This is called when the user selects an item.
                          setState(() {
                            controller.dropdownValue4 = value!;
                          });
                        },
                        items: [
                          'How do you know about WWAD 2022',
                          'Print Media',
                          'Television',
                          'Social Media',
                          'Calls/Email',
                          'Friends/Relatives'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Icon(Icons.arrow_drop_down_outlined),
                  ],
                ),
              ),

              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Checkbox(
                      value: controller.isChecked,
                      onChanged: (value) {
                        controller.isChecked = !controller.isChecked;
                        setState(() {});
                      }),
                  Wrap(
                    children: [
                      RichText(
                          text: TextSpan(
                        children: [
                          TextSpan(
                              text:
                                  "I hereby confirm that the above information provided\nby me is correct, and that I have read and will abide by\nthe",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 10)),
                          TextSpan(
                              text: " Noteworthy Points ",
                              style:
                                  TextStyle(color: Colors.blue, fontSize: 10),
                              recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              print('Text Clicked');
                            }),
                          TextSpan(
                            text: "mentioned here.",
                            style: TextStyle(color: Colors.black, fontSize: 10),
                          )
                        ],
                      ))
                    ],
                  )
                ],
              ),

              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  SizedBox(
                    child: ElevatedButton(
                      style:
                          ElevatedButton.styleFrom(primary: Color(0xff0aaaa0)),
                      onPressed: () {
                        print(controller.isChecked.toString());
                        print(controller.flag.toString());
                        print(controller.applictionCont.toString());

                        if (controller.participantName.text.isEmpty ||
                            controller.pcontaxtNo.text.isEmpty ||
                            controller.pemail.text.isEmpty ||
                            controller.guardianName.text.isEmpty ||
                            controller.dropdownValue.toString() ==
                                "Age (10 to 45 years)" ||
                            controller.purpose.text.isEmpty ||
                            controller.stateLocation.text.isEmpty ||
                            controller.aboutYourSelf.text.isEmpty ||
                            controller.healthIssue.text.isEmpty ||
                            controller.assistance.text.isEmpty ||
                            controller.dropdownValue4.toString() ==
                                "Please mention Disability Category" ||
                            controller.dropdownValue2.toString() ==
                                "How do you know about WWAD 2022" ||
                            controller.talentShowDes.text.isEmpty ||
                            controller.image1 == null ||
                            controller.image2 == null) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("Required Field Missing! 1 ")));
                        } else {
                          if (controller.flag == false) {
                            if (controller.institutionName.text.isEmpty ||
                                controller.authPersonName.text.isEmpty ||
                                controller.iemail.text.isEmpty ||
                                controller.icontactNo.text.isEmpty ||
                                controller.addressLocation.text.isEmpty ||
                                controller.noOfParticipation.text.isEmpty) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                      content:
                                          Text("Required Field Missing! 2")));
                            } else {
                              if (controller.isChecked == true) {
                                controller
                                    .registrationApiCall(context)
                                    .then((value) {
                                  setState(() {});
                                });
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                        content:
                                            Text("Please Confirm T&C !!")));
                              }
                            }
                          } else {
                            if (controller.applictionCont >= 1) {
                              print("this is working");
                              if (controller.isChecked == true) {
                                controller
                                    .registrationStudentApiCall(context)
                                    .then((value) {
                                  setState(() {});
                                });
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                        content:
                                            Text("Please Confirm T&C !!")));
                              }
                            } else {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext) => AlertDialog(
                                        title: Text(
                                          "Participants Status",
                                          style: TextStyle(fontSize: 12),
                                        ),
                                        content: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Text(
                                              "Your participants request\n count completed !!",
                                              textAlign: TextAlign.center,
                                            )
                                          ],
                                        ),
                                        actions: [
                                          SizedBox(
                                            height: 40,
                                            child: ElevatedButton(
                                                style: ElevatedButton.styleFrom(
                                                    primary: Color(0xffee5c2b)),
                                                onPressed: () {
                                                  Get.off(HomeView());
                                                },
                                                child: Text("Cancel")),
                                          )
                                        ],
                                      ));
                            }
                          }
                        }
                      },
                      child: Text("Next"),
                    ),
                  ),
                ],
              ),

              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
