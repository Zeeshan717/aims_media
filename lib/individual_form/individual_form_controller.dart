import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../services/api_service.dart';

class IndividualFormController extends GetxController {
  TextEditingController participantName = TextEditingController();
  TextEditingController guardianName = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController contaxtNo = TextEditingController();
  //TextEditingController age = TextEditingController();
  TextEditingController stateLocation = TextEditingController();
  // TextEditingController aadharImg = TextEditingController();
  TextEditingController purpose = TextEditingController();
  TextEditingController aboutYourSelf = TextEditingController();
 // TextEditingController talentShow = TextEditingController();
  TextEditingController talentShowDes = TextEditingController();
//  TextEditingController disability = TextEditingController();
  // TextEditingController udidImag = TextEditingController();
  TextEditingController healthIssue = TextEditingController();
  TextEditingController assistance = TextEditingController();
//  TextEditingController knowAbout = TextEditingController();


  String dropdownValue ="Age (10 to 45 years)";
  String dropdownValue2 ="No";
  String dropdownValue3 ="Please mention Disability Category";
  String dropdownValue4 ="How do you know about WWAD 2022";

  bool isChecked = false;

  File? image1;
  File? image2;

  List<String> getAgeArray()
  {
     List<String> a =[];
    for(int i=10;i<=45;i++)
      {
        a.add("$i");
      }
    return a;
  }

  Future uploadFromGallery1() async {
    // ignore: deprecated_member_use
    PickedFile? pickedFile = await ImagePicker().getImage(source: ImageSource.gallery, imageQuality: 30);
    if (pickedFile != null) {
      image1 = File(pickedFile.path);
    }
  }
  Future uploadFromGallery2() async {
    // ignore: deprecated_member_use
    PickedFile? pickedFile = await ImagePicker().getImage(source: ImageSource.gallery, imageQuality: 30);
    if (pickedFile != null) {
      image2 = File(pickedFile.path);
    }
  }

  Future registrationApiCall(BuildContext context) async {

    EasyLoading.show(status:"");
    await ApiService.registrationApi(
        "Individual",
        "",
        "",
        "",
        "",
        "",
        "",
        participantName.text,
        contaxtNo.text,
        email.text,
        guardianName.text,
        dropdownValue.toString(),
        purpose.text,
        stateLocation.text,
        aboutYourSelf.text,
        dropdownValue3.toString(),
        healthIssue.text,
        assistance.text,
        dropdownValue4.toString(),
        dropdownValue2.toString(),
        talentShowDes.text,
        File(image1!.path),File(image2!.path),
        ).then((value) {
      if(value["success"].toString()==1.toString())
      {
        EasyLoading.dismiss();
        showDialog(
            context: context,
            builder: (BuildContext) => AlertDialog(
              title: Text("Registration Status",style: TextStyle(
                  fontSize: 12
              ),),

              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [

                  Text("Your registration has been \nsuccessfully completed!!",textAlign: TextAlign.center,)

                ],
              ),
              actions: [
                SizedBox(
                  height: 40,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Color(0xffee5c2b)),
                      onPressed: () {
                        Get.back();
                        Get.back();
                        Get.back();
                        clearText();
                        // update();
                      },
                      child: Text("Cancel")),
                )
              ],
            ));
      }else{
        EasyLoading.dismiss();
      }
    });
  }

  void clearText()
  {
    participantName.clear();
    contaxtNo.clear();
    email.clear();
    guardianName.clear();
    dropdownValue= "Age (10 to 45 years)";
    purpose.clear();
    stateLocation.clear();
    aboutYourSelf.clear();
    dropdownValue3 ="Please mention Disability Category" ;
    healthIssue.clear();
    assistance.clear();
    dropdownValue4= "How do you know about WWAD 2022"  ;
    dropdownValue2= "No" ;
    talentShowDes.clear();
    image1 = null;
    image2 = null;

  }
}
