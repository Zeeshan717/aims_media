import 'dart:convert';
import 'dart:io';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import '../model/details_model.dart';

class ApiService {
  static const BaseUrl = "https://aims.media/wwad_api/";

  static Future registrationApi(
    String usertype,
    String insname,
    String asname,
    String email,
    String phoneno,
    String address,
    String studentno,
    String sname,
    String phone,
    String stuemail,
    String parentname,
    String dob,
    String purpose,
    String statedetail,
    String about_yourself,
    String disablility,
    String medical_issue,
    String assistance_required,
    String know_about,
    String talent_show,
    String talent_show_detail,
    File aadharFileName,
    File uuidFileName,
  ) async {
    // print('${usertype}');
    // print('insname ${insname}');
    // print('asname ${asname}');
    // print('email ${email}');
    // print('phoneno ${phoneno}');
    // print('address ${address}');
    // print('studentno ${studentno}');
    // print('sname ${sname}');
    // print('${phone}');
    // print('${stuemail}');
    // print('${parentname}');
    // print('${dob}');
    // print('${purpose}');
    // print('${statedetail}');
    // print('${about_yourself}');
    // print('${disablility}');
    // print('${medical_issue}');
    // print('${assistance_required}');
    // print('${know_about}');
    // print('${talent_show}');
    // print('${talent_show_detail}');
    // print('${aadharFileName}');
    // print('${uuidFileName}');

    var postUri = Uri.parse("${BaseUrl}registration");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['usertype'] = usertype;
    request.fields['insname'] = insname;
    request.fields['asname'] = asname;
    request.fields['email'] = email;
    request.fields['phoneno'] = phoneno;
    request.fields['address'] = address;
    request.fields['studentno'] = studentno;
    request.fields['sname'] = sname;
    request.fields['phone'] = phone;
    request.fields['stuemail'] = stuemail;
    request.fields['parentname'] = parentname;
    request.fields['dob'] = dob;

    request.fields['purpose'] = purpose;
    request.fields['statedetail'] = statedetail;
    request.fields['about_yourself'] = about_yourself;
    request.fields['disablility'] = disablility;
    request.fields['medical_issue'] = medical_issue;
    request.fields['assistance_required'] = assistance_required;
    request.fields['know_about'] = know_about;
    request.fields['talent_show'] = talent_show;
    request.fields['talent_show_detail'] = talent_show_detail;

    try {
      request.files.add(await http.MultipartFile.fromPath(
          "upload_dob_certificate", aadharFileName.path));
    } catch (e) {
      print(e.toString());

      // request.fields['upload_dob_certificate'] = "";
    }
    try {
      request.files.add(await http.MultipartFile.fromPath(
          "upload_certificate", uuidFileName.path));
    } catch (e) {
      print(e.toString());
      // request.fields['upload_dob_certificate'] = "";
    }

    http.Response response =
        await http.Response.fromStream(await request.send());

    return jsonDecode(response.body);
  }

  static Future registrationStudentApi(
    String sname,
    String phone,
    String email,
    String parentname,
    String dob,
    String Institution_id,
    String purpose,
    String statedetail,
    String about_yourself,
    String disablility,
    String medical_issue,
    String assistance_required,
    String know_about,
    String talent_show,
    String talent_show_detail,
    File aadharFileName,
    File uuidFileName,
  ) async {
    print("--------");
    var postUri = Uri.parse("${BaseUrl}registration_student");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['sname'] = sname;
    request.fields['phone'] = phone;
    request.fields['email'] = email;
    request.fields['parentname'] = parentname;
    request.fields['dob'] = dob;
    try {
      request.fields['Institution_id'] = Institution_id;
    } catch (e) {
      print('00000000');
    }
    request.fields['purpose'] = purpose;
    request.fields['statedetail'] = statedetail;
    request.fields['about_yourself'] = about_yourself;
    request.fields['disablility'] = disablility;
    request.fields['medical_issue'] = medical_issue;
    request.fields['assistance_required'] = assistance_required;
    request.fields['know_about'] = know_about;
    request.fields['talent_show'] = talent_show;
    request.fields['talent_show_detail'] = talent_show_detail;

    try {
      request.files.add(await http.MultipartFile.fromPath(
          "upload_dob_certificate", aadharFileName.path));
    } catch (e) {
      print(e.toString());

      // request.fields['upload_dob_certificate'] = "";
    }
    try {
      request.files.add(await http.MultipartFile.fromPath(
          "upload_certificate", uuidFileName.path));
    } catch (e) {
      print(e.toString());
      // request.fields['upload_dob_certificate'] = "";
    }

    http.Response response =
        await http.Response.fromStream(await request.send());

    print("-------" + response.toString());

    return jsonDecode(response.body);
  }

  static Future<DetailsModel> getDetailsApi(String email, String contactNo) async{
    print(email);
    print(contactNo);
    var response = await http.post(Uri.parse("${BaseUrl}get_registration_detail"),body: {
    "email_id":email,
    "contact_no":contactNo
    });
    print(response.body.toString());
    return detailsModelFromJson(response.body);
  }
  // static Future<> getParticipantData(String institutionId) async {
  //   final response = await http.post(Uri.parse("${BaseUrl}get_institute_detail"),
  //       body: {"Institution_id": institutionId,});
  //   if (response.statusCode == 200) {
  //     return ppfAccountModelFromJson(response.body);
  //   }
  //   return ppfAccountModelFromJson(response.body);
  // }

}
