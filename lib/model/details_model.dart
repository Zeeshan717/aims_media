// To parse this JSON data, do
//
//     final detailsModel = detailsModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

DetailsModel detailsModelFromJson(String str) => DetailsModel.fromJson(json.decode(str));

String detailsModelToJson(DetailsModel data) => json.encode(data.toJson());

class DetailsModel {
  DetailsModel({
    required this.studentDetail,
    required this.instituteName,
    required this.associateName,
    required this.userEmail,
    required this.userPhone,
    required this.address,
    required this.success,
    required this.message,
  });

  List<StudentDetail> studentDetail;
  String instituteName;
  String associateName;
  String userEmail;
  String userPhone;
  String address;
  String success;
  String message;

  factory DetailsModel.fromJson(Map<String, dynamic> json) => DetailsModel(
    studentDetail: json["student_detail"]==null?[]:List<StudentDetail>.from(json["student_detail"].map((x) => StudentDetail.fromJson(x))),
    instituteName: json["institute_name"]==null?"":json["institute_name"],
    associateName: json["associate_name"]==null?"":json["associate_name"],
    userEmail: json["user_email"]==null?"":json["user_email"],
    userPhone: json["user_phone"]==null?"":json["user_phone"],
    address: json["address"]==null?"":json["address"],
    success: json["success"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "student_detail": List<dynamic>.from(studentDetail.map((x) => x.toJson())),
    "institute_name": instituteName,
    "associate_name": associateName,
    "user_email": userEmail,
    "user_phone": userPhone,
    "address": address,
    "success": success,
    "message": message,
  };
}

class StudentDetail {
  StudentDetail({
    required this.studentName,
    required this.guardianName,
    required this.studentAge,
    required this.studentContact,
    required this.emailId,
    required this.location,
    required this.registrationId,
    required this.registrationDate,
  });

  String studentName;
  String guardianName;
  String studentAge;
  String studentContact;
  String emailId;
  String location;
  String registrationId;
  String registrationDate;

  factory StudentDetail.fromJson(Map<String, dynamic> json) => StudentDetail(
    studentName: json["student_name"],
    guardianName: json["guardian_name"],
    studentAge: json["student_age"],
    studentContact: json["student_contact"],
    emailId: json["email_id"],
    location: json["location"],
    registrationId: json["registration_id"],
    registrationDate: json["registration_date"],
  );

  Map<String, dynamic> toJson() => {
    "student_name": studentName,
    "guardian_name": guardianName,
    "student_age": studentAge,
    "student_contact": studentContact,
    "email_id": emailId,
    "location": location,
    "registration_id": registrationId,
    "registration_date": registrationDate,
  };
}
