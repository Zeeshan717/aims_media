import 'package:aims_media/model/details_model.dart';
import 'package:aims_media/services/api_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailsController extends GetxController{

  TextEditingController emailCtr = TextEditingController();
  TextEditingController contactNoCtr = TextEditingController();
  DetailsModel? detailsModel;


Future getDetailsApiCall(BuildContext context) async{

  try{
    await ApiService.getDetailsApi(emailCtr.text, contactNoCtr.text).then((value) {
      if(value.success.toString() == "1"){
        print(value);
        detailsModel = value;
      }else{
        detailsModel = null;
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(value.message)));
      }
    });
  }catch(e){
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("No internet connection")));
  }


}
}