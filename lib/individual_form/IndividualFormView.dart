import 'dart:io';

import 'package:aims_media/services/api_service.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'individual_form_controller.dart';

enum ImageSourceType { gallery, camera }

class IndividualFormView extends StatefulWidget {
  const IndividualFormView({Key? key}) : super(key: key);

  @override
  State<IndividualFormView> createState() => _IndividualFormViewState();
}

class _IndividualFormViewState extends State<IndividualFormView> {
  var controller = Get.put(IndividualFormController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.chevron_left_outlined,
              color: Colors.black,
            ),
            onPressed: () {
              Get.back();
              Get.back();
            },
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            'Registration',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                letterSpacing: 2.5),
          )),
      body: Container(
        padding: EdgeInsets.all(16),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("Participant Detail",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff989a9c),
                      )),
                ],
              ),
              SizedBox(height: 20),

              // Participant Name
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Participant Name",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.participantName,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "Participant Name",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),

              // Guardian Name
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Guardian Name",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.guardianName,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "Guardian Name",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),

              // Email-Id
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Email-Id",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.email,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "Email-Id",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Contact No",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    keyboardType: TextInputType.number,
                    controller: controller.contaxtNo,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "Contact No",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Age",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  border: Border.all(
                    width: 1,
                    color: Color(0xff000000).withOpacity(.20),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: DropdownButton<String>(
                        iconSize: 0,
                        value: controller.dropdownValue,
                        elevation: 16,
                        style: TextStyle(color: Colors.black),
                        underline: Container(
                          height: 0,
                          color: Colors.deepPurpleAccent,
                        ),
                        focusColor: Color(0xfffafafa).withOpacity(.30),
                        autofocus: false,
                        onChanged: (String? value) {
                          // This is called when the user selects an item.
                          setState(() {
                            controller.dropdownValue = value!;
                          });
                        },
                        items: ['Age (10 to 45 years)',...controller.getAgeArray()]
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Icon(Icons.arrow_drop_down_outlined),
                  ],
                ),
              ),
              // Container(
              //     padding: EdgeInsets.symmetric(horizontal: 16),
              //     decoration: BoxDecoration(
              //       border: Border.all(width: 1, color: Color(0xff979BA3)),
              //     ),
              //     child: const TextField(
              //       style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
              //       cursorColor: Colors.black,
              //       cursorWidth: 1.5,
              //       decoration: InputDecoration(
              //         border: InputBorder.none,
              //         enabledBorder: InputBorder.none,
              //         focusedBorder: InputBorder.none,
              //         hintText: "Age",
              //         hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
              //       ),
              //     )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "State Location",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.stateLocation,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "State Location",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Upload Aadhar Card",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  controller
                      .uploadFromGallery1()
                      .then((value) => setState(() {}));
                },
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Color(0xff979BA3)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 14),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(" Tap To Upload Image"),
                          controller.image1 != null
                              ? Image.file(
                                  controller.image1!,
                                  width: 100.0,
                                  height: 100.0,
                                  fit: BoxFit.fitHeight,
                                )
                              : Container(
                                  child: Icon(Icons.image),
                                )
                        ],
                      ),
                    )),
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Statement of Purpose",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    onChanged: (value){

                      setState(() {

                      });
                    },
                    controller: controller.purpose,
                    keyboardType: TextInputType.multiline,
                    minLines: 1, //Normal textInputField will be displayed
                    maxLines: 5, // when user presses enter it will adapt to it
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText:
                          "Statement of Purpose (SOP) (Why do you want to register in WWAD 2022?) (100 Words)",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                      counterText:"${controller.purpose.text.split(" ").length - 1}/100"
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "About Yourself",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                   onChanged: (value){
                     setState((){});
                   } ,
                    controller: controller.aboutYourSelf,
                    minLines: 1,
                    maxLines: 5,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText:
                          "About Yourself (Please cover your skills, hobbies, area of interest and what you are currently doing) (100 Words)",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                        counterText:"${controller.aboutYourSelf.text.split(" ").length - 1}/100"
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Have you participated in any talent show/event in the past?",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  border: Border.all(
                    width: 1,
                    color: Color(0xff000000).withOpacity(.20),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: DropdownButton<String>(
                        iconSize: 0,
                        value: controller.dropdownValue2,
                        elevation: 16,
                        style: TextStyle(color: Colors.black),
                        underline: Container(
                          height: 0,
                          color: Colors.deepPurpleAccent,
                        ),
                        focusColor: Color(0xfffafafa).withOpacity(.30),
                        autofocus: false,
                        onChanged: (String? value) {
                          // This is called when the user selects an item.
                          setState(() {
                            controller.dropdownValue2 = value!;
                          });
                        },
                        items: ['Yes', 'No']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Icon(Icons.arrow_drop_down_outlined),
                  ],
                ),
              ),

              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Talent Show Description",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(

                     readOnly: controller.dropdownValue2 == "Yes"?false:true,
                    controller: controller.talentShowDes,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(

                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "Talent show/event Description",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Please mention Disability Category",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  border: Border.all(
                    width: 1,
                    color: Color(0xff000000).withOpacity(.20),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: DropdownButton<String>(
                        iconSize: 0,
                        value: controller.dropdownValue3,
                        elevation: 16,
                        style: TextStyle(color: Colors.black),
                        underline: Container(
                          height: 0,
                          color: Colors.deepPurpleAccent,
                        ),
                        focusColor: Color(0xfffafafa).withOpacity(.30),
                        autofocus: false,
                        onChanged: (String? value) {
                          // This is called when the user selects an item.
                          setState(() {
                            controller.dropdownValue3 = value!;
                          });
                        },
                        items: [
                          'Please mention Disability Category',
                          'Autism Spectrum Disorder',
                          'Down Syndrome',
                          'Cerebral Palsy',
                          'Locomotor Disability',
                          'Dwarfism',
                          'Visually Impaired/Blindness',
                          'Acid Victims',
                          'Intellectual Disability',
                          'Multiple Disabilities including deafblindness',
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Icon(Icons.arrow_drop_down_outlined),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "UUID Card",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  controller
                      .uploadFromGallery2()
                      .then((value) => setState(() {}));
                },
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Color(0xff979BA3)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 14),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(" Tap To Upload Image"),
                          controller.image2 != null
                              ? Image.file(
                                  controller.image2!,
                                  width: 100.0,
                                  height: 100.0,
                                  fit: BoxFit.fitHeight,
                                )
                              : Container(
                                  child: Icon(Icons.image),
                                )
                        ],
                      ),
                    )),
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Medical/Health Issue, if any",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.healthIssue,
                    minLines: 1,
                    maxLines: 5,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: "Medical/Health Issue, if any",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Assistance",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                  ),
                  child: TextField(
                    controller: controller.assistance,
                    minLines: 1,
                    maxLines: 5,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                    cursorColor: Colors.black,
                    cursorWidth: 1.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText:
                          "If any specific assistance required, please mention",
                      hintStyle: TextStyle(fontSize: 12, letterSpacing: 0.5),
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "How do you know about WWAD 2022",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  border: Border.all(
                    width: 1,
                    color: Color(0xff000000).withOpacity(.20),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: DropdownButton<String>(
                        iconSize: 0,
                        value: controller.dropdownValue4,
                        elevation: 16,
                        style: TextStyle(color: Colors.black),
                        underline: Container(
                          height: 0,
                          color: Colors.deepPurpleAccent,
                        ),
                        focusColor: Color(0xfffafafa).withOpacity(.30),
                        autofocus: false,
                        onChanged: (String? value) {
                          // This is called when the user selects an item.
                          setState(() {
                            controller.dropdownValue4 = value!;
                          });
                        },
                        items: [
                          'How do you know about WWAD 2022',
                          'Print Media',
                          'Television',
                          'Social Media',
                          'Calls/Email',
                          'Friends/Relatives'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Icon(Icons.arrow_drop_down_outlined),
                  ],
                ),
              ),


              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Checkbox(
                      value: controller.isChecked,
                      onChanged: (value) {
                        controller.isChecked = !controller.isChecked;
                        setState(() {});
                      }),
                  Wrap(
                    children: [
                      RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                  text:
                                  "I hereby confirm that the above information provided\nby me is correct, and that I have read and will abide by\nthe",
                                  style:
                                  TextStyle(color: Colors.black, fontSize: 10)),
                              TextSpan(
                                  text: " Noteworthy Points ",
                                  style:
                                  TextStyle(color: Colors.blue, fontSize: 10),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      print('Text Clicked');
                                    }),
                              TextSpan(
                                text: "mentioned here.",
                                style: TextStyle(color: Colors.black, fontSize: 10),
                              )
                            ],
                          ))
                    ],
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  SizedBox(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Color(0xff0aaaa0)),
                      onPressed: () {
                        if (controller.participantName.text.isEmpty ||
                            controller.contaxtNo.text.isEmpty ||
                            controller.email.text.isEmpty ||
                            controller.guardianName.text.isEmpty ||
                            controller.dropdownValue.toString() ==
                                "Age (10 to 45 years)" ||
                            controller.purpose.text.isEmpty ||
                            controller.stateLocation.text.isEmpty ||
                            controller.aboutYourSelf.text.isEmpty ||
                            controller.healthIssue.text.isEmpty ||
                            controller.assistance.text.isEmpty ||
                            controller.dropdownValue4.toString() ==
                                "Please mention Disability Category" ||
                            controller.dropdownValue2.toString() ==
                                "How do you know about WWAD 2022" ||
                            controller.talentShowDes.text.isEmpty ||
                            controller.image1 == null ||
                            controller.image2 == null) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("Required Field Missing!  ")));
                        } else {
                          if (controller.isChecked == true) {
                            controller.registrationApiCall(context);
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Text("Please Confirm T&C !!")));
                          }
                        }
                      },
                      child: Text("Submit"),
                    ),
                  ),
                ],
              ),

              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
