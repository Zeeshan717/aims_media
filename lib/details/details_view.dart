
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'details_controller.dart';

class DetailsView extends StatefulWidget {
  const DetailsView({Key? key}) : super(key: key);

  @override
  State<DetailsView> createState() => _DetailsViewState();
}

class _DetailsViewState extends State<DetailsView> {
  var controller = Get.put(DetailsController());
  bool isRunning = false;

  @override
  void initState() {

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("Application Details",style: TextStyle(letterSpacing: 0.5),),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "Login ",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 12),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  border:
                  Border.all(width: 1, color: Color(0xff979BA3)),
                ),
                child: TextField(
                  controller: controller.emailCtr,
                  style: TextStyle(

                      fontSize: 12, fontWeight: FontWeight.w400),
                  cursorColor: Colors.black,
                  cursorWidth: 1.5,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    hintText: "Enter your Email Address",
                    hintStyle:
                    TextStyle(fontSize: 12, letterSpacing: 0.5),
                  ),
                )),
            SizedBox(height: 10),
            SizedBox(
              height: 10,
            ),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  border:
                  Border.all(width: 1, color: Color(0xff979BA3)),
                ),
                child: TextField(
                  controller: controller.contactNoCtr,
                  style: TextStyle(
                      fontSize: 12, fontWeight: FontWeight.w400),
                  cursorColor: Colors.black,
                  cursorWidth: 1.5,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    hintText: "Enter your Contact number",
                    hintStyle:
                    TextStyle(fontSize: 12, letterSpacing: 0.5),
                  ),
                )),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                    height: 40,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: Color(0xff7f5384)),
                        onPressed: () async{
                         if(controller.emailCtr.text.trim() == "" || controller.contactNoCtr.text.trim()==""){
                           ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("All fields required !")));
                         }else{
                           setState(() {
                             isRunning = true;
                           });
                           await controller.getDetailsApiCall(context).then((value) => setState((){}));
                           setState(() {
                             isRunning = false;
                           });
                         }
                           },
                        child: Text("GET DETAILS"))),
                Container(
                  width: 25,
                  height: 25,
                  child: Visibility(
                    visible: isRunning,
                      child: CircularProgressIndicator(
                        color: Color(0xff7f5384),
                      )),
                )
              ],

            ),
            SizedBox(height: 20),
            controller.detailsModel != null ?
            controller.detailsModel!.associateName==""?Column(
              children: [
                Container(
                    padding:
                    EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    decoration: BoxDecoration(
                      border:
                      Border.all(width: 1, color: Color(0xff0aaaa0)),
                    ),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Name : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.studentDetail[0].studentName,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Guardian Name : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.studentDetail[0].guardianName,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Age : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.studentDetail[0].studentAge,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Email-Id : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.studentDetail[0].emailId,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Contact No : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.studentDetail[0].studentContact,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Address & Location : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.studentDetail[0].location,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Registration Id : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.studentDetail[0].registrationId,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Registration Date : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.studentDetail[0].registrationDate,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        )
                        /*  Row(
                          children: [
                            Text(
                              "Number Of Participant : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              controller.detailsModel!.,
                              style: TextStyle(fontSize: 10),
                            )
                          ],
                        ),*/
                      ],
                    )),
                SizedBox(height: 20),
                 /* Container(
                    constraints: BoxConstraints(maxHeight: 200),
                    // height: 300,
                    padding:
                    EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    decoration: BoxDecoration(
                      border:
                      Border.all(width: 1, color: Color(0xff0aaaa0)),
                    ),
                    child: DataTable2(
                        columnSpacing: 12,
                        horizontalMargin: 0,
                        minWidth: 200,
                        columns: [
                          DataColumn2(
                            fixedWidth: 50,
                            label: Text('SNo.'),
                            size: ColumnSize.L,
                          ),
                          DataColumn(
                            label: Text('Name'),
                          ),
                          DataColumn(
                            label: Text('Phone No.'),
                          ),
                          DataColumn(
                            label: Text('Registration Id'),
                          ),
                        ],
                        rows: List<DataRow>.generate(
                            10,
                                (index) => DataRow(cells: [
                              DataCell(Text("${index + 1}")),
                              DataCell(Text("Anshul Sharma")),
                              DataCell(Text("985658792")),
                              DataCell(Text("225498"))
                            ])))),*/
                SizedBox(height: 20),
              ],
            ):
            Column(
              children: [
                Container(
                    padding:
                    EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    decoration: BoxDecoration(
                      border:
                      Border.all(width: 1, color: Color(0xff0aaaa0)),
                    ),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Institution Name : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.associateName,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Authorized Person Name : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.instituteName,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Email-Id : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.userEmail,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Contact No : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.userPhone,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Address & Location : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: Text(
                                controller.detailsModel!.address,
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          ],
                        ),
                      /*  Row(
                          children: [
                            Text(
                              "Number Of Participant : ",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              controller.detailsModel!.,
                              style: TextStyle(fontSize: 10),
                            )
                          ],
                        ),*/
                      ],
                    )),
                SizedBox(height: 20),
                Container(
                   constraints: BoxConstraints(minHeight: 0,
                   maxHeight: MediaQuery.of(context).size.height*.39),

                    padding:
                    EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    decoration: BoxDecoration(
                      border:
                      Border.all(width: 1, color: Color(0xff0aaaa0)),
                    ),
                    child: DataTable2(
                         headingRowHeight: 40,
                        columnSpacing: 12,
                        horizontalMargin: 0,
                        minWidth: 200,
                        columns: [
                          DataColumn2(
                            fixedWidth: 50,
                            label: Text('SNo.'),
                            size: ColumnSize.L,
                          ),
                          DataColumn(
                            label: Text('Name'),
                          ),
                          DataColumn(
                            label: Text('Phone No.'),
                          ),
                          DataColumn(
                            label: Text('Registration Id'),
                          ),
                        ],
                        rows: List<DataRow>.generate(
                            controller.detailsModel!.studentDetail.length,
                                (index) => DataRow(cells: [
                              DataCell(Text("${index + 1}")),
                              DataCell(Text(controller.detailsModel!.studentDetail[index].studentName,style:TextStyle(
                                fontSize: 12
                              ))),
                              DataCell(Text(controller.detailsModel!.studentDetail[index].studentContact,style:TextStyle(
                                  fontSize: 12
                              ))),
                              DataCell(Text(controller.detailsModel!.studentDetail[index].registrationId,style:TextStyle(
                                  fontSize: 12
                              )))
                            ])))),
                SizedBox(height: 20),
              ],
            ):Container(

            ),
          ],
        ),
      ),
    );
  }
}
