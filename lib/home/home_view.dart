import 'package:aims_media/individual_form/IndividualFormView.dart';
import 'package:aims_media/institution_form/InstitutionFormview.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:get/get.dart';

import '../details/details_view.dart';

// final List<String> imgList = [
//   'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
//   'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
//   'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
//   'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
//   'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
//   'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
// ];

final List<String> imgList =[
  "assets/slider/wwad12.jpg",
  "assets/slider/wwad13.jpg",
  "assets/slider/wwad14.jpg",
  "assets/slider/wwad15.jpg",
  "assets/slider/wwad16.jpg",
  "assets/slider/wwad17.jpg",
];

final List<String> logoList =[
  "assets/logos/Advisory1.jpg",
  "assets/logos/Advisory2.jpg",
  "assets/logos/Advisory3.jpg",
  "assets/logos/Advisory4.jpg",
  "assets/logos/Advisory5.jpg",
  "assets/logos/Advisory6.jpg",

];

bool isVisible1 = false;
bool isVisible2 = false;
bool isVisible3 = false;


class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final List<Widget> imageSliders = imgList
      .map((item) => Container(
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Stack(
                    children: <Widget>[
                      Image.asset(item, fit: BoxFit.cover, width: 1000.0),
                      Positioned(
                        bottom: 0.0,
                        left: 0.0,
                        right: 0.0,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color.fromARGB(200, 0, 0, 0),
                                Color.fromARGB(0, 0, 0, 0)
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                            ),
                          ),
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 20.0),
                          child: Text('',
                          //  'No. ${imgList.indexOf(item)} image',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          ))
      .toList();

  int _current = 0;
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      //appBar: AppBar(backgroundColor: Color(0xff0aaaa0), title: Text('AIMS')),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.asset("assets/logos/logo.jpg",scale: 3,),
              Text('',style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold, letterSpacing: 2.5),),
            ],
          ),
        actions: [
      //    IconButton(onPressed: (){}, icon: Icon(Icons.info_outline,color: Colors.,))
        ],
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          CarouselSlider(
            items: imageSliders,
            carouselController: _controller,
            options: CarouselOptions(
                autoPlay: true,
                enlargeCenterPage: true,
                aspectRatio: 2.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: imgList.asMap().entries.map((entry) {
              return GestureDetector(
                onTap: () => _controller.animateToPage(entry.key),
                child: Container(
                  width: 6.0,
                  height: 6.0,
                  margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: (Theme.of(context).brightness == Brightness.dark
                              ? Colors.white
                              : Colors.black)
                          .withOpacity(_current == entry.key ? 0.9 : 0.4)),
                ),
              );
            }).toList(),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Card(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Walk With A Difference",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, letterSpacing: 0.5),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Walk With A Difference (WWAD) - is a social initiative of AIMS Media to discover the hidden talents of"
                      " persons with disabilities in the fields of Clothing, Fashion, Art, Music, Dance and Sports through social "
                      "events and exhibitions. WWAD aspires to become the medium through which the disabled get to "
                      "showcase their abilities and experience the happiness of being looked for what they possess instead of"
                      "their weakness/disability.",
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "WWAD - Fashion and Disability Walking With A Difference!",
                      style: TextStyle(fontSize: 10),
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Card(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "WWAD was organized in Delhi and Bangalore in 2015 and 2016 at Siri Fort Auditorium Delhi and Christ University "
                "Bangalore respectively and through a multidisciplinary approach helped thousands of persons with special needs "
                "to meet the challenges. In the past, WWAD witnessed by various policy makers, bureaucrats, corporates"
                "institutions and individuals sensitized millions of people across the country towards people with special"
                " needs and also launched Disabled Mannequins. Our vision is to create a society which is empathetic towards the PwDs, "
                          "supports Equality and Inclusion and does not shrink from giving them an opportunity to create a niche for themselves and walk with their heads held high. Also help in exploring new arenas for employment for the disabled and lay some emphasis on their needs and requirements.",
                      style: TextStyle(fontSize: 12, letterSpacing: 0.5),
                      textAlign: TextAlign.start,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
             padding: EdgeInsets.all(16),
             child: Container(
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                border: Border.all(width: 1,color:Color(0xff89608e)),
                borderRadius: BorderRadius.circular(4)

              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Season 1 - WWAD 2015 "Delhi"',style: TextStyle(color:Color(0xff89608e),fontWeight:FontWeight.bold),),
                      InkWell(
                          onTap:(){
                            isVisible1 = !isVisible1;
                            setState(() {});
                          } ,
                          child:isVisible1?Icon(Icons.remove):Icon(Icons.add))
                    ],
                  ),
                  Visibility(
                    visible:isVisible1 ,
                    child: Padding(
                      padding: const EdgeInsets.only(top:8.0),
                      child: Text(
                        "WWAD 2015 was hosted at Siri Fort Auditorium, Delhi. More than 600 participants appeared for the "
                            "auditions held at Bosco Public School, New Delhi and 30 finalists were selected to walk the ramp. The"
                            "finale was graced by the presence of Sh. Ramesh Bhiduri and Smt. Meenakshi Lekhiji.",style: TextStyle(
                        fontSize: 10
                      ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(16),
            child: Container(
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                  border: Border.all(width: 1,color:Color(0xff89608e)),
                  borderRadius: BorderRadius.circular(4)

              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Season 2 - WWAD 2016 "Bengaluru"',style: TextStyle(color:Color(0xff89608e),fontWeight:FontWeight.bold),),
                      InkWell(
                          onTap:(){
                            isVisible2 = !isVisible2;
                            setState(() {});
                          } ,
                          child:isVisible2?Icon(Icons.remove):Icon(Icons.add))
                    ],
                  ),
                  Visibility(
                    visible:isVisible2 ,
                    child: Padding(
                      padding: const EdgeInsets.only(top:8.0),
                      child: Text(
                        "WWAD 2016 was held at Christ University, Bengaluru. More than 800 persons with diverse needs "
                            "appeared as participants and got their names on the hearts of thousands of audience. Many "
                            "prominent news channels visited for covering the event and providing exposure to the talents of PwD's.",style: TextStyle(
                          fontSize: 10
                      ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(16),
            child: Container(
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                  border: Border.all(width: 1,color:Color(0xff89608e)),
                  borderRadius: BorderRadius.circular(4)

              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,

                    children: [
                      Text('Season 3 - WWAD 2022 "Kolkata"',style: TextStyle(color:Color(0xff89608e),fontWeight:FontWeight.bold),),
                      InkWell(
                          onTap:(){
                            isVisible3 = !isVisible3;
                            setState(() {});
                          } ,
                          child:isVisible3?Icon(Icons.remove):Icon(Icons.add))
                    ],
                  ),
                  Visibility(
                    visible:isVisible3 ,
                    child: Padding(
                      padding: const EdgeInsets.only(top:8.0),
                      child: Text(
                        "We wish to bring different seasons of WWAD every year to different cities of the country and this"
                            "year in 2022 we are bringing the third season of WWAD in Kolkata, India; where people with "
                            "disabilities from all over the country will be called to participate in the auditions and this year we "
                            "expecting over 900 registrations; whereas thirty finalists after a meticulous training by the "
                            "professional will perform / showcase their hidden talent in one of the prestigious and premier "
                            "auditorium in Kolkata - principally in Science City.",style: TextStyle(
                          fontSize: 10
                      ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text("Honorary Advisory Board")],
          ),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  for (var i in logoList)
                    Row(
                      children: [
                        CircleAvatar(

                          radius: 30, // Image radius
                          backgroundImage: AssetImage(i,),
                        ),
                        SizedBox(
                          width: 15,
                        )
                      ],
                    )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 40,
          ),
          SizedBox(
              height: 50,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Color(0xff7f5384)),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext) => AlertDialog(
                              title: Text("Register As",style: TextStyle(
                                fontSize: 12
                              ),),
                              content: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [

                                  SizedBox(
                                    height:40 ,
                                    child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            primary: Color(0xff0aaaa0)),
                                        onPressed: () {
                                          Get.to(InstitutionFormView());
                                        },
                                        child: Text("Institution")),
                                  ),
                                  SizedBox(
                                    height: 40,
                                    child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            primary: Color(0xffee5c2b)),
                                        onPressed: () {
                                          Get.to(IndividualFormView());
                                        },
                                        child: Text("Individual")),
                                  )
                                ],
                              ),
                              actions: [],
                            ));
                  },
                  child: Text("Registration for WWAD 2022 Auditions"))),
          SizedBox(
            height: 20,
          ),
          SizedBox(
              height: 50,
              child:
              TextButton(
                  child: Text(
                      "    Get  Your  Registration  Details    ".toUpperCase(),
                      style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold)
                  ),
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
                      foregroundColor: MaterialStateProperty.all<Color>( Color(0xff7f5384)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: BorderSide(color:  Color(0xff7f5384))
                          )
                      )
                  ),
                  onPressed: () {
                    Get.to(DetailsView());
                  } 
              )),
          SizedBox(
            height: 40,
          ),
        ]),
      ),
    );
  }
}

/*
class CarouselWithIndicatorDemo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorState();
  }
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicatorDemo> {
  int _current = 0;
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Carousel with indicator controller demo')),
      body: Column(children: [
        Expanded(
          child: CarouselSlider(
            items: imageSliders,
            carouselController: _controller,
            options: CarouselOptions(
                autoPlay: true,
                enlargeCenterPage: true,
                aspectRatio: 2.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: imgList.asMap().entries.map((entry) {
            return GestureDetector(
              onTap: () => _controller.animateToPage(entry.key),
              child: Container(
                width: 12.0,
                height: 12.0,
                margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: (Theme.of(context).brightness == Brightness.dark
                        ? Colors.white
                        : Colors.black)
                        .withOpacity(_current == entry.key ? 0.9 : 0.4)),
              ),
            );
          }).toList(),
        ),
      ]),
    );
  }
}*/
